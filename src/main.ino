// NOTE Driver Windows : http://en.doit.am/CH341SER.zip
#include <TM1637Display.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <time.h>
#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>

// Définition du pinout de la demoboard
// Entrée analogique
#define A0                    0
// Entrée/Sortie numérique
#define D0                    16
#define D1                    5
#define D2                    4
#define D3                    0
#define D4                    2
#define D5                    14
#define D6                    12
#define D7                    13
#define D8                    15

// NOTE : Installer la bibliothèque Adafruit Unified Sensor Library: https://github.com/adafruit/Adafruit_Sensor
#include <Adafruit_Sensor.h>
// NOTE : Installer la bibliothèque DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
#include <DHT.h>
#include <DHT_U.h>

#define LED_EXTERN            D0
#define LED_ROUGE             D5
#define LED_JAUNE             D6
#define LED_VERT              D8


#define DHTPIN                D4         // Pin which is connected to the DHT sensor.
#define DHTTYPE               DHT11      // DHT 11

#define CLK,

// Initialisation du capteur de température & humidité
DHT_Unified DHT_SENSOR(DHTPIN, DHTTYPE);
#include <ESP8266WiFi.h>

//affichage
#include <LiquidCrystal_I2C.h>
int lcdColumns = 16;
int lcdRows = 2;
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);

#ifndef STASSID
#define STASSID "wisClAvnir"
#define STAPSK  "01928374"
#endif
WiFiUDP udp;
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "0.europe.pool.ntp.org";
const char * ssid = STASSID; // your network SSID (name)
const char * pass = STAPSK;  // your network password

//infra
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
const long utcOffsetInSeconds = 3600;
unsigned int localPort = 2390;      // local port to listen for UDP packets

const uint16_t kRecvPin = 13;

IRrecv irrecv(kRecvPin);

decode_results results;

void setup() {
  pinMode(LED_ROUGE, OUTPUT);
  pinMode(LED_JAUNE, OUTPUT);
  pinMode(LED_VERT, OUTPUT);


  Serial.begin(115200);

  //infra
    irrecv.enableIRIn();  // Start the receiver
    while (!Serial)  // Wait for the serial connection to be establised.
      delay(50);
    Serial.println();
    Serial.print("IRrecvDemo is now running and waiting for IR message on Pin ");
  Serial.println(kRecvPin);

	// initialize LCD
  lcd.init();
  // turn on LCD backlight
  lcd.backlight();
  Serial.begin(115200);

  DHT_SENSOR.begin();

  WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }

      udp.begin(localPort);

}

void loop() {


  if (irrecv.decode(&results)) {
      // print() & println() can't handle printing long longs. (uint64_t)
      serialPrintUint64(results.value, HEX);
      irrecv.resume();  // Receive the next value
    }
  delay(100);

  sensors_event_t event;
  digitalWrite(LED_ROUGE, HIGH);
  digitalWrite(LED_JAUNE, HIGH);
  digitalWrite(LED_VERT, HIGH);


  DHT_SENSOR.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    DEBUG_ESP_PORT.println("Error reading temperature!");
  } else {
    DEBUG_ESP_PORT.print("Temperature: ");
    DEBUG_ESP_PORT.print(event.temperature);
    DEBUG_ESP_PORT.println(" *C");
  }


  sensors_event_t eventdeux;
  // Get humidity event and print its value.
  DHT_SENSOR.humidity().getEvent(&eventdeux);
  if (isnan(eventdeux.relative_humidity)) {
    DEBUG_ESP_PORT.println("Error reading humidity!");
  } else {
    DEBUG_ESP_PORT.print("Humidity: ");
    DEBUG_ESP_PORT.print(eventdeux.relative_humidity);
    DEBUG_ESP_PORT.println("%");
  }

	// set cursor to first column, first row
  lcd.setCursor(0, 0);
  lcd.print("Projet IOT");
  lcd.setCursor(0,1);
  lcd.print("EPSI B3");
  digitalWrite(LED_JAUNE, HIGH);
  digitalWrite(LED_VERT, HIGH);
  digitalWrite(LED_ROUGE, LOW);
  delay(5000);
  // clears the display to print new message
  lcd.clear();
  // set cursor to first column, second row
  lcd.setCursor(0, 0);
  lcd.print("Humidity :");
  lcd.setCursor(10,0);
  lcd.print(eventdeux.relative_humidity);

  lcd.setCursor(0, 1);
  lcd.print("Temp :");
  lcd.setCursor(10,1);
  lcd.print(event.temperature);

  digitalWrite(LED_ROUGE, HIGH);
  digitalWrite(LED_VERT, HIGH);
  digitalWrite(LED_JAUNE, LOW);

  delay(5000);
  lcd.clear();

  WiFi.hostByName(ntpServerName, timeServerIP);

  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(200);
  int hour;
  int min;
  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no packet yet");
  } else {
    Serial.print("packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
	unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
  unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
	unsigned long secsSince1900 = highWord << 16 | lowWord;
     Serial.print("Seconds since Jan 1 1900 = ");
     Serial.println(secsSince1900);

     // now convert NTP time into everyday time:
     Serial.print("Unix time = ");
     // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
     const unsigned long seventyYears = 2208988800UL;
     // subtract seventy years:
     unsigned long epoch = secsSince1900 - seventyYears;
		 min = (epoch  % 3600) / 60;

		 hour = ((epoch  % 86400L) / 3600+2);



}
lcd.setCursor(0, 0);
lcd.print("heure :");
lcd.setCursor(11,0);
lcd.print(hour);
lcd.setCursor(0, 1);
lcd.print("minutes :");
lcd.setCursor(11,1);
lcd.print(min);

digitalWrite(LED_ROUGE, HIGH);
digitalWrite(LED_JAUNE, HIGH);
digitalWrite(LED_VERT, LOW);

delay(5000);
lcd.clear();

}


void sendNTPpacket(IPAddress& address) {

  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}
